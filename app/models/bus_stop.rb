class BusStop < ActiveRecord::Base
  has_many :bus_timetables, ->{order(:depart_time)}
  accepts_nested_attributes_for :bus_timetables, allow_destroy: true, reject_if: :all_blank

  validates :name, presence: true
end
