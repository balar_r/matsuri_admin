class Good < ActiveRecord::Base
  has_many :good_anothers
  accepts_nested_attributes_for :good_anothers, allow_destroy: true, reject_if: :all_blank
  has_many :exhibits
  has_many :shops, :through => :exhibits

  validates :name, presence: true
end
