class Group < ActiveRecord::Base
  has_many :clubs
  has_many :group_anothers
  has_many :shops
  accepts_nested_attributes_for :group_anothers, allow_destroy: true, reject_if: :all_blank

  validates :name, presence: true
end
