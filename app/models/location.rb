class Location < ActiveRecord::Base
  has_many :projects
  has_many :clubs
end
