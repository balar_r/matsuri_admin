class Shop < ActiveRecord::Base
  belongs_to :group
  has_many :exhibits
  has_many :goods, :through => :exhibits
  accepts_nested_attributes_for :exhibits, allow_destroy: true, reject_if: :all_blank

  validates :name, presence: true
end
