json.array!(@clubs) do |club|
  json.extract! club, :id, :start_time, :end_time, :name, :group_id, :location_id, :introduction, :keyword
  json.url club_url(club, format: :json)
end
