json.array!(@projects) do |project|
  json.extract! project, :id, :start_time, :end_time, :edited_start_time, :edited_end_time, :title, :location_id, :introduction, :tweet_interval, :keyword
  json.url project_url(project, format: :json)
end
