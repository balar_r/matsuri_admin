json.array!(@shops) do |shop|
  json.extract! shop, :id, :name, :group_id, :booth
  json.url shop_url(shop, format: :json)
end
