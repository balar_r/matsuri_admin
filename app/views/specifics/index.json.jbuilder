json.array!(@specifics) do |specific|
  json.extract! specific, :id, :reaction_word, :return_str
  json.url specific_url(specific, format: :json)
end
