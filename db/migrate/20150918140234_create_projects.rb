class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.datetime :edited_start_time
      t.datetime :edited_end_time
      t.string :title
      t.integer :location_id
      t.text :introduction
      t.integer :tweet_interval
      t.text :keyword

      t.timestamps null: false
    end
  end
end
