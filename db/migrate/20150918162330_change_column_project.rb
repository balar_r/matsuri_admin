class ChangeColumnProject < ActiveRecord::Migration
  def change
    change_column_null :projects, :start_time, false
    change_column_null :projects, :end_time, false
    change_column_null :projects, :title, false
    change_column_null :projects, :location_id, false

    change_column_default :projects, :edited_start_time, nil
    change_column_default :projects, :edited_end_time, nil
    change_column_default :projects, :tweet_interval, 0 

    change_column_null :locations, :name, false
  end
end
