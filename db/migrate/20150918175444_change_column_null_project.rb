class ChangeColumnNullProject < ActiveRecord::Migration
  def change
    change_column_null :projects, :start_time, true
    change_column_null :projects, :end_time, true
    change_column_null :projects, :title, true
    change_column_null :projects, :location_id, true

  end
end
