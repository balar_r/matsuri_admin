class CreateGroupAnothers < ActiveRecord::Migration
  def change
    create_table :group_anothers do |t|
      t.integer :group_id
      t.string :name

      t.timestamps null: false
    end
  end
end
