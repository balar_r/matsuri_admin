class CreateGoodAnothers < ActiveRecord::Migration
  def change
    create_table :good_anothers do |t|
      t.integer :good_id
      t.string :name

      t.timestamps null: false
    end
  end
end
