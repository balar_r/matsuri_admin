class CreateClubs < ActiveRecord::Migration
  def change
    create_table :clubs do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.string :name
      t.integer :group_id
      t.integer :location_id
      t.text :introduction
      t.text :keyword

      t.timestamps null: false
    end
  end
end
