class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :name
      t.integer :group_id
      t.integer :booth

      t.timestamps null: false
    end
  end
end
