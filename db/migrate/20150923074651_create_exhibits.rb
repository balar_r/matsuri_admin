class CreateExhibits < ActiveRecord::Migration
  def change
    create_table :exhibits do |t|
      t.integer :shop_id
      t.integer :good_id

      t.timestamps null: false
    end
  end
end
