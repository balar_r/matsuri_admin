class CreateSpecifics < ActiveRecord::Migration
  def change
    create_table :specifics do |t|
      t.string :reaction_word
      t.text :return_str

      t.timestamps null: false
    end
  end
end
