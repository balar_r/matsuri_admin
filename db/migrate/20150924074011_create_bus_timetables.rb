class CreateBusTimetables < ActiveRecord::Migration
  def change
    create_table :bus_timetables do |t|
      t.integer :bus_stop_id
      t.time :depart_time

      t.timestamps null: false
    end
  end
end
