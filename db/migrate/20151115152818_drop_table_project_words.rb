class DropTableProjectWords < ActiveRecord::Migration
  def change
    drop_table :project_words
    drop_table :club_words
  end
end
