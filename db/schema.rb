# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151115152818) do

  create_table "bus_stops", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bus_timetables", force: :cascade do |t|
    t.integer  "bus_stop_id"
    t.time     "depart_time"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "chats", force: :cascade do |t|
    t.string   "regword"
    t.text     "word"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clubs", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.string   "name"
    t.integer  "group_id"
    t.integer  "location_id"
    t.text     "introduction"
    t.text     "keyword"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "exhibits", force: :cascade do |t|
    t.integer  "shop_id"
    t.integer  "good_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "good_anothers", force: :cascade do |t|
    t.integer  "good_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "goods", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "group_anothers", force: :cascade do |t|
    t.integer  "group_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", force: :cascade do |t|
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "edited_start_time"
    t.datetime "edited_end_time"
    t.string   "title"
    t.integer  "location_id"
    t.text     "introduction"
    t.integer  "tweet_interval",    default: 0
    t.text     "keyword"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "shops", force: :cascade do |t|
    t.string   "name"
    t.integer  "group_id"
    t.integer  "booth"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "specifics", force: :cascade do |t|
    t.string   "reaction_word"
    t.text     "return_str"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

end
