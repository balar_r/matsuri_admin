require 'test_helper'

class ProjectsControllerTest < ActionController::TestCase
  setup do
    @project = projects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:projects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create project" do
    assert_difference('Project.count') do
      post :create, project: { edited_end_time: @project.edited_end_time, edited_start_time: @project.edited_start_time, end_time: @project.end_time, introduction: @project.introduction, keyword: @project.keyword, location_id: @project.location_id, start_time: @project.start_time, title: @project.title, tweet_interval: @project.tweet_interval }
    end

    assert_redirected_to project_path(assigns(:project))
  end

  test "should show project" do
    get :show, id: @project
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @project
    assert_response :success
  end

  test "should update project" do
    patch :update, id: @project, project: { edited_end_time: @project.edited_end_time, edited_start_time: @project.edited_start_time, end_time: @project.end_time, introduction: @project.introduction, keyword: @project.keyword, location_id: @project.location_id, start_time: @project.start_time, title: @project.title, tweet_interval: @project.tweet_interval }
    assert_redirected_to project_path(assigns(:project))
  end

  test "should destroy project" do
    assert_difference('Project.count', -1) do
      delete :destroy, id: @project
    end

    assert_redirected_to projects_path
  end
end
